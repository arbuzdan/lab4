import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import services.LoggerService;
import services.MailService;
import services.SlackService;

@Configuration
public class AppConfig {
    @Bean
    public Logger logger(){
        return new Logger(loggerService());
    }

    @Bean
    public LoggerService loggerService() {
        //от этого зависит какой сервис будет использован
        return new SlackService();
    }
}
